var myLati = 50;
var myLong = 10;
var myAlti = 100;
var myTemp = 273.15;
var myPres = 1013;
var myTime = new Date();
var locvalue = "Frankfurt";
const apiKey = "1bfd0cd4677a15909d901207d81f7490";

function chkInput(event) {
  var keyinp = event.code;
  if (keyinp == "Enter") {
    changeLoc();
  }
}

function doAction() {
  getLocation();
}

function getHomeSensor() {
  getWeatherData();
  getGasData();
  getUVIndex();
}
//
// Alerting the Info-Block
function getInfo() {
  alert('The webApp shows currrent air pollution data, temperature and huminity data. gathered by: \n' +
        'https://openweathermap.org/api \n \n' + 
        'Environment data are collected based on your location: \n' +
        '- Different toxic gases in µg/m^3 and ppb \n' +
        '- Dust load size PM10/PM2.5 and general Air Quality \n' +
        '- Sun intensity (inicdence angle) and UV-Index. \n' +
        '- Temperature in Celsius and Range \n' +
        '- Humidity and general weather state \n' +
        '- Pressure and wind velocity \n \n' +
        'Thresholds for the quality are orientated at the EU recommendations \n' +
        'https://www.airqualitynow.eu/about_indices_definition.php \n');
}
//
// Alerting the Help-Block
function getHelp() {
  alert( '1. Press "Settings" to recalculate for new location  \n' +
         '2. Press "Catalog" to print the data \n' +
         '3. Press "About" for a short explanation. \n' +
         '4. Press "Help" for this info. \n' +
         '5. Press "Home" to reload the webApp. \n' +
         ' \n' +
         'Known Bugs: \n' +
         '- Stil None'); 
}

function changeLoc() {
    // read input Feld
    var newLoc = document.getElementById('myLoc').value; 
    console.log(newLoc);
    // search for location
 //    http://api.openweathermap.org/geo/1.0/direct?q=Kairo&limit=5&appid={API key}
 fetch(`   https://api.openweathermap.org/geo/1.0/direct?q=${newLoc}&limit=1&appid=${apiKey}
 `)
 .then(
   function(response) {
     console.log(response.status)
     if (response.status !== 200) {
       console.log('Looks like there was a problem. Status Code: ' +
         response.status);
       return;
     }
     // Examine the text in the response
     response.json().then(function(data) {
       console.log(data);   
       if (data != "") {
// get new posiitions
        myLati = data[0].lat;
        myLong = data[0].lon;
        locvalue = document.getElementById('myLoc').value;
        console.log(myLati,myLong);
        getHomeSensor();
        drawMap();
       }
       if (data == "") {
         alert("Location not found. Restore last Location.");
         document.getElementById('myLoc').value = locvalue;
       }

     });
   }
 )
.catch(err => {
 console.log(err);
});

}

function getGasData() {

  var aq = {"text": ["Good","Fair","Moderate","Poor", "Very poor"]};

  fetch(`https://api.openweathermap.org/data/2.5/air_pollution?lat=${myLati}&lon=${myLong}&appid=${apiKey}`)
  .then(
    function(response) {
      console.log(response.status)
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' +
          response.status);
        return;
      }
      // Examine the text in the response
      response.json().then(function(data) {
        console.log(data);       

        txtout("Loading..","c_covalue"); 
        txtout("Loading..","c_noxvalue"); 
        txtout("Loading..","c_o3value"); 
        txtout("Loading..","c_so2value");    
        txtout("Loading..","c_nh3value");  

        var aqvalue = data.list[0].main.aqi-1;
        var covalue = data.list[0].components.co; 
        var novalue = data.list[0].components.no; 
        var no2value = data.list[0].components.no2; 
        var o3value = data.list[0].components.o3; 
        var so2value = data.list[0].components.so2; 
        var nh3value = data.list[0].components.nh3;     
        var pm2value = data.list[0].components.pm2_5; 
        var pm10value = data.list[0].components.pm10;  
        var pmvalue = (pm2value + pm10value)/2; 

        noxvalue = no2value + novalue;
        txtout(aq.text[aqvalue],"c_aqvalue");
        txtout(form(covalue,1),"c_covalue");        
        txtout(form(noxvalue,1),"c_noxvalue");
        txtout(form(o3value,1),"c_o3value");
        txtout(form(so2value,1),"c_so2value");
        txtout(form(nh3value,1),"c_nh3value");
        txtout(form(pm10value,0)+" / "+form(pm2value,0),"c_pmvalue");

        document.getElementById("coMeter").setAttribute("value", covalue);
        document.getElementById("noxMeter").setAttribute("value", noxvalue);
        document.getElementById("o3Meter").setAttribute("value", o3value);
        document.getElementById("so2Meter").setAttribute("value", so2value);
        document.getElementById("nh3Meter").setAttribute("value", nh3value);
        document.getElementById("pmMeter").setAttribute("value", pmvalue);    
        

//
// calculate the ppb-Value
// Multiply with facto molvolume/molmass
// molvolume Vgas= 22.7 l/mol bei 0°C und 1013 hPa
//  V(real) = Vgas*P1/P2*T2/T1

        var molvol = 1013/myPres*273.15/myTemp*22.7;
        noxppb = novalue*(molvol/30) + no2value*(molvol/46);
        txtout(form(covalue*molvol/28,0),"c_coppb");
        txtout(form(noxppb,0),"c_noxppb");
        txtout(form(o3value*molvol/48,0),"c_o3ppb");
        txtout(form(so2value*molvol/64,0),"c_so2ppb");
        txtout(form(nh3value*molvol/17,0),"c_nh3ppb");

      });
    }
  )
  .catch(err => {
    console.log(err);
  });
  
  }


  // https://api.openweathermap.org/data/2.5/onecall?lat=49.8&lon=8.6&exclude=hourly,daily&appid=1bfd0cd4677a15909d901207d81f7490

  function getWeatherData() {

    fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${myLati}&lon=${myLong}&appid=${apiKey}`)
//   fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${myLati}&lon=${myLong}&exclude=hourly,daily&appid=1bfd0cd4677a15909d901207d81f7490`)
    .then(
      function(response) {
        console.log(response.status)
        if (response.status !== 200) {
          console.log('Looks like there was a problem. Status Code: ' +
            response.status);
          return;
        }
        // Examine the text in the response
        response.json().then(function(data) {
          console.log(data);        
          
          myTemp = data.main.temp;
          myPres = data.main.pressure;

         txtout("Loading..","c_t1value"); 
         var t1value = data.main.temp-273.15;  
         txtout(form(t1value,1)+" °C","c_t1value");
         var t1meter = Math.abs(t1value-20);
         document.getElementById("t1Meter").setAttribute("value", t1meter);
         var difftemp = (data.main.temp_max - data.main.temp_min)/2;
         txtout("+/-"+form(difftemp,1)+" °C","c_difftemp");

         txtout("Loading..","c_h1value"); 
         var h1value = data.main.humidity;  
         txtout(form(h1value,1)+" %","c_h1value");
         var h1meter = Math.abs(h1value-50);
         document.getElementById("h1Meter").setAttribute("value", h1meter);

         txtout("Loading..","c_d1value"); 
         var d1value = data.main.pressure;  
         txtout(form(d1value,0),"c_d1value");

         var winvalue = data.wind.speed;
         if (data.wind.gust > 0) {
          winvalue = Math.max(data.wind.speed,data.wind.gust)
         }
          txtout(form(winvalue*3.6,0)+ " km/h","c_winvalue");  
          var w1meter = winvalue;
          document.getElementById("w1Meter").setAttribute("value", w1meter);     

          var skyvalue = data.weather[0].main;
          txtout(skyvalue,"c_skyvalue"); 

          myTime = new Date();
          document.getElementById("timestamp").innerHTML = myTime;

        });
      }
    )
    .catch(err => {
      console.log(err);
    });
    
    }


    function getUVIndex() {

    fetch(`https://api.openweathermap.org/data/3.0/onecall?lat=${myLati}&lon=${myLong}&exclude=minutely,hourly,daily&appid=${apiKey}`)
      .then(
        function(response) {
          console.log(response.status)
          if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' +
              response.status);
            return;
          }
          // Examine the text in the response
          response.json().then(function(data) {
            console.log(data);   

            var uvivalue = data.current.uvi;
            txtout(form(uvivalue,1)+" UV","c_uvivalue"); 
            var uvimeter = uvivalue;
            document.getElementById("uviMeter").setAttribute("value", uvimeter);            
//
// Approximation to calculate the sun incident
// Assuming sinus-curve between sunrise and sunset (0-1-0) for the day arc
// Assuming sinus-curve for the seasonal arc (spring 0 summer 1 autumn 0 winter -1)
// earth-axis 23.44° 
            var sunvalue = 0;
            var tnow = data.current.dt;
            var sunup = data.current.sunrise;
            var sundn = data.current.sunset;

            if (tnow > sunup) {
              if (tnow < sundn) {
                sunvalue = Math.sin((tnow-sunup)/(sundn-sunup)*Math.PI)
              }
            }
                        
            var yearday = form(365/12*myTime.getMonth()+365/12*myTime.getDate()/31,0)-81;
            var sunhigh = 23.44*Math.sin(yearday/365*Math.PI*2);
            var sundeg  = 90 - myLati + sunhigh;

            txtout(form(sundeg*sunvalue,1) + "°","c_sunvalue"); 
            
          });
        }
      )
            .catch(err => {
              console.log(err);
            });
            
            }

  

    function getLocationData() {

      fetch(`   https://api.openweathermap.org/geo/1.0/reverse?lat=${myLati}8&lon=${myLong}&limit=1&appid=${apiKey}
      `)
      .then(
        function(response) {
          console.log(response.status)
          if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' +
              response.status);
            return;
          }
          // Examine the text in the response
          response.json().then(function(data) {
            console.log(data);    

          locvalue = data[0].name;
          document.getElementById("myLoc").defaultValue = locvalue;
 //         txtout(locvalue,"c_locvalue"); 

          });
        }
      )
    .catch(err => {
      console.log(err);
    });

  }

  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
      alert('Geolocation is not supported by this browser.');
    }
  }

  function showPosition(position) {
    myLati = position.coords.latitude;
    myLong = position.coords.longitude;
    myAlti = position.coords.altitude;
    console.log(position);
    drawMap();
    getLocationData();
    getWeatherData()
    getGasData();
    getUVIndex();

  }

  function drawMap() {
    
    txtout(form(myLati ,3)+" °","c_latvalue"); 
    txtout(form(myLong ,3)+" °","c_lonvalue"); 
    txtout(form(myAlti ,0)+" m","c_altvalue");    


    vmbox = myLati - 0.001;
    vpbox = myLati + 0.001;
    hmbox = myLong - 0.001;
    hpbox = myLong + 0.001;
    locurl = "https://www.openstreetmap.org/export/embed.html?bbox="+hmbox+"%2C"+vmbox+"%2C"+hpbox+"%2C"+vpbox;
//    locurl = "https://www.openstreetmap.org/export/#map=16/49.8185/8.6441&layers=H";
    document.getElementById("mapholder").src = locurl;
  
    console.log(locurl);
  }

//
// Formatting Number
function form(x, z) {
  var y;
  x = Math.round(x * power(10, z));
  y = x / power(10, z);
  return y;
}

//
// Formatting text output
function txtout(txtstr,elemstr) {
var list = document.getElementById(elemstr);
while (list.hasChildNodes()) {
  list.removeChild(list.firstChild);
}
var wrtline = document.createTextNode(txtstr);
var element = document.getElementById(elemstr).appendChild(wrtline);
}
//
// Power-Function
function power(x, z) {
  var y;
  y = Math.pow(x, z);
  return y;
}
//
// Root-Function
function sqrt(x) {
  var y;
  y = Math.sqrt(x);
  return y;
}

//
// Cos-Function in Degrees
function cos(x) {
  var y;
  y = Math.cos(x * Math.PI / 180);
  return y;
}
//
// Sin-Function in Degrees
function sin(x) {
  var y;
  y = Math.sin(x * Math.PI / 180);
  return y;
}


// https://api.openweathermap.org/data/2.5/air_pollution?lat=50&lon=9&appid=1bfd0cd4677a15909d901207d81f7490
